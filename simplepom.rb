require 'libnotify'
require 'active_support/time'
require 'action_view'
include ActionView::Helpers::DateHelper

cycle_counter = 0
pomodoro_time = 25
short_break = 5
long_break = 15

while true
  begin
    `tmux set-option -g status-bg blue`
    puts "Starting..."
    cycle_counter += 1
    end_time = Time.now + pomodoro_time.minutes
    while Time.now < end_time
      remaining_time_in_words = distance_of_time_in_words(Time.now,end_time)
      print "\rWork Time Remaining: #{remaining_time_in_words}                                                  "
      `purple-remote "setstatus?status=unavailable&message=Possibly available in #{remaining_time_in_words}"`
      sleep 5.seconds
    end

    `purple-remote "setstatus?status=unavailable&message="`
    `tmux set-option -g status-bg red`

    break_length = short_break
    if cycle_counter == 4 
      break_length = long_break
      cycle_counter = 0
    end

    Libnotify.show :body => "Time to start your #{break_length} minute break", :summary => 'Pomodoro Complete', :urgency => :low

    puts "\nPress enter to start #{break_length} minute break timer.                                                        "
    gets
    `tmux set-option -g status-bg green`

    end_time = Time.now + break_length.minutes
    while Time.now < end_time 
      remaining_time_in_words = distance_of_time_in_words(Time.now,end_time)
      `purple-remote "setstatus?status=available&message=Available for #{remaining_time_in_words}"`
      print "\rBreak Time Remaining: #{remaining_time_in_words}                                                  "
      sleep 5.seconds
    end
    `purple-remote "setstatus?status=available&message="`
    `tmux set-option -g status-bg red`

    Libnotify.show :body => 'Time to get back to work', :summary => 'Break Over', :urgency => :low

    puts "\nPress enter to start pomodoro timer.                                                   "
    gets
  ensure
    `purple-remote "setstatus?status=available&message="`
  end

end
